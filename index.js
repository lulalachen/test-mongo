const Twitter = require('twitter')
const MongoClient = require('mongodb').MongoClient
require('dotenv').config()

const { DB_URL, DB_NAME, DB_USERNAME, DB_PASSWORD } = process.env

var client = new Twitter({
  consumer_key: process.env.CONSUMER_KEY,
  consumer_secret: process.env.CONSUMER_SECRET,
  access_token_key: process.env.ACCESS_TOKEN_KEY,
  access_token_secret: process.env.ACCESS_TOKEN_SECRET,
})

client.get(
  'statuses/user_timeline',
  { screen_name: 'lulalachen' },
  (error, tweets, response) => {
    if (!error) {
      // tweets.map(({ text }) => text).map(console.log)
    }
  },
)

// Connection URL
const url = `mongodb://${DB_USERNAME}:${DB_PASSWORD}@${DB_URL}/${DB_NAME}`
// Database Name
const dbName = DB_NAME

// Use connect method to connect to the server
MongoClient.connect(
  url,
  function(err, client) {
    if (err) {
      console.log(err)
    } else {
      console.log('Connected successfully to server')
      const db = client.db(dbName)
      const collection = db.collection('test')
      // Insert some documents
      collection
        .find({ a: 1 })
        .toArray()
        .then(console.log)

      collection
        .insertMany([{ a: 1 }, { a: 2 }, { a: 3 }])
        .then(console.log)
        .catch(console.error)

      client.close()
    }
  },
)
